FROM tmidas/root6
WORKDIR /

#Install dependencies
RUN yum install -y cmake g++ gcc libexpat1-dev \
libxerces-c-dev libx11-dev libgl1-mesa-dev xerces-c xerces-c-devel libXmu libXmu-devel lesstif-devel

#Build geant4
#RUN mkdir geant4
RUN git clone --branch geant4-10.5-release --depth 1 https://github.com/Geant4/geant4.git
#WORKDIR geant4

RUN mkdir build
WORKDIR build
RUN cmake3 -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_BUILD_TYPE=RelWithDebInfo -DGEANT4_INSTALL_DATA=ON -DCLHEP_ROOT_DIR=$CLHEP_BASE_DIR -DCMAKE_COMPILER_IS_GNUCXX=ON -DGEANT4_USE_OPENGL_X11=ON -DGEANT4_USE_RAYTRACER_X11=ON -DGEANT4_USE_GDML=ON ../geant4 2>&1 | tee cmake.log
RUN make -j4
RUN make install

ENV G4NEUTRONHPDATA /usr/local/share/Geant4-10.5.1/data/G4NDL4.5
ENV G4LEDATA /usr/local/share/Geant4-10.5.1/data/G4EMLOW7.7
ENV G4LEVELGAMMADATA /usr/local/share/Geant4-10.5.1/data/PhotonEvaporation5.3
ENV G4RADIOACTIVEDATA /usr/local/share/Geant4-10.5.1/data/RadioactiveDecay5.3
ENV G4PARTICLEXSDATA /usr/local/share/Geant4-10.5.1/data/G4PARTICLEXS1.1
ENV G4PIIDATA /usr/local/share/Geant4-10.5.1/data/G4PII1.3
ENV G4REALSURFACEDATA /usr/local/share/Geant4-10.5.1/data/RealSurface2.1.1
ENV G4SAIDXSDATA /usr/local/share/Geant4-10.5.1/data/G4SAIDDATA2.0
ENV G4ABLADATA /usr/local/share/Geant4-10.5.1/data/G4ABLA3.1
ENV G4INCLDATA /usr/local/share/Geant4-10.5.1/data/G4INCL1.0
ENV G4ENSDFSTATEDATA /usr/local/share/Geant4-10.5.1/data/G4ENSDFSTATE2.2

WORKDIR /
CMD /bin/bash
